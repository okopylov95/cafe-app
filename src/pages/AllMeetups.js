import { useContext } from "react";
import MeetupContext from "../store/meetups-context";
import MeetUpList from "../components/meetups/MeetupList";
import data from "../data/meetups.json"


function AllMeetupsPage() {
  const meetupCtx = useContext(MeetupContext);

  return (
    <section>
      <h1>The best Establishments in Lviv</h1>
      <MeetUpList meetups={meetupCtx.meetups.concat(data)} />
    </section>
  );
}

export default AllMeetupsPage;
