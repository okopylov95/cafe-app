import { useHistory } from "react-router-dom";
import { useContext } from "react";
import MeetupContext from "../store/meetups-context";
import NewMeetupForm from "../components//meetups/NewMeetupForm";


function NewMeetupPage() {
  const history = useHistory();
  const meetupCtx = useContext(MeetupContext);

  function addMeetupHandler(meetupData) {
    console.log(meetupData);
    meetupCtx.addMeetup(meetupData);
    

    history.replace("/");
  }

  return (
    <section>
      <h1>Add New Establishment</h1>
      <NewMeetupForm onAddMeetup={addMeetupHandler} />
    </section>
  );
}

export default NewMeetupPage;
