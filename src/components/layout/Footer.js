import classes from "./Footer.module.css";

const Footer = () => {
  return (
    <div className={classes["footer"]}>
      <div className={classes["text"]}>
        <h1>contact</h1>
        <a href="mailto:okopylov95@gmail.com">okopylov95@gmail.com</a>
        <p>Location: Lviv</p>
      </div>
    </div>
  );
};

export default Footer;
