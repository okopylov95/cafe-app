import { createContext, useState } from "react";

const MeetupContext = createContext({
  meetups: [],
  addMeetup: (meetup) => {},
});

export function MeetupContextProvider(props) {
  const [meetups, setMeetups] = useState([]);

  function addMeetup(meetup) {
    setMeetups((prevMeetups) => {
      return prevMeetups.concat(meetup);
    });
  }
  
  const context = {
    meetups: meetups,
    addMeetup: addMeetup,
  };

  return (
    <MeetupContext.Provider value={context}>
      {props.children}
    </MeetupContext.Provider>
  );
}

export default MeetupContext;
